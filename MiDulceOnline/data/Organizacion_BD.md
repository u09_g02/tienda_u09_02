# tienda_U09_02
Base de datos:

Nombre de la BD: MiDulceOnline

Colecciones:

    clientes:
                id: String,
                userName: String,
                contrasena: String,
                correo: String,
                nombre: String,
                telefono: String,
                fechaNacimiento: Date,
                pais: String,
                direccion: String
    
    productos:
                id: String,
                categoria: String,
                nombre: String,
                marca: String
                precio: Number,
                imagen: String,
                estado: Boolean

    encabezado_facturas:
                        id: String,
                        fecha: Date,
                        id_cliente: String,
                        activo: Boolean

    detalle_facturas:
                        id_encabezado_producto: String
                        productos: Array [{
                            id_productos: String,
                            cantidad: Number,
                            total: Number
                        }] 