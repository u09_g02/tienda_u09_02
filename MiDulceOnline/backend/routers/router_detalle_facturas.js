const express = require('express');
const router = express.Router();
const controller_detalle_factura = require('../controllers/controller_detalle_facturas.js');

router.get('/listar', controller_detalle_factura);
router.get('/cargar/:id', controller_detalle_factura);
router.put('/editar/:id', controller_detalle_factura);
router.post('/agregar', controller_detalle_factura);
router.delete('/borrar/:id', controller_detalle_factura);

module.exports = router;