const express = require('express');
const router = express.Router();
const controllerClientes = require('../controllers/controller_clientes.js');

router.get('/listar', controllerClientes);
router.get('/cargar/:id', controllerClientes);
router.get('/findByUserName/:userName', controllerClientes);
router.put('/editar/:id', controllerClientes);
router.post('/agregar', controllerClientes);
router.delete('/borrar/:id', controllerClientes);

module.exports = router;