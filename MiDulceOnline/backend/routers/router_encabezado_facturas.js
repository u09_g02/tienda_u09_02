const express = require('express');
const router = express.Router();
const controladorEncabezadoFacturas = require('../controllers/controller_encabezado_facturas');

router.get("/listar",controladorEncabezadoFacturas);
router.get("/cargar/:id",controladorEncabezadoFacturas);
router.post("/agregar",controladorEncabezadoFacturas);
router.post("/editar/:id",controladorEncabezadoFacturas);
router.delete("/borrar/:id",controladorEncabezadoFacturas);

module.exports = router; 