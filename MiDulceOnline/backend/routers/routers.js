const express = require('express');
const router = express.Router();

// Importar rutas
const routerProductos = require('./router_productos');
const routerClientes = require('./router_clientes');
const routerEncabezadoFacturas = reqiere('./router_encabezado_facturas.js');
const routerClientesEncabezados = reqire('./router_clientesencabezado.js');
const routerDetalleFacturas = reqire('./router_detalle_facturas.js');


// Uso de turas
router.use('/productos', routerProductos);
router.use('/clientes', routerClientes);
router.use('/encabezado_facturas', routerEncabezadoFacturas);
router.use('/clientesFacturas', routerClientesEncabezados);
router.use('/detalle_facturas', routerDetalleFacturas);
module.exports = router;