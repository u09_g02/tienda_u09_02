const express = require('express');
const router = express.Router();

const modeloEncabezadoFacturas = require('../models/model_encabezado_facturas');

router.get('/listar', (req, res) => {
    modeloEncabezadoFacturas.find({},function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.get('/cargar/:id', (req, res) => {
    modeloEncabezadoFacturas.find({id:req.params.id},function(docs,err)
    {
        if(!err)
        {
            res.send(docs);
        }
        else
        {
            res.send(err);
        }
    })
});

router.post('/agregar',(req,res)=>{
    const nuevoEncabezadoFacturas = new modeloEncabezadoFacturas({

        id: req.body.id,
        fecha: req.body.fecha,
        id_cliente: req.body.id_cliente,
        activo: req.body.activo
    });
    nuevoEncabezadoFacturas.save(function(err)
    {
        if(!err)
        {
            res.send("El registro se agregó exitosamente");
        }
        else
        {
            res.send(err.stack);
        }
    }
    );
});

router.post('/editar/:id',(req,res)=>{
    modeloEncabezadoFacturas.findOneAndUpdate({id:req.params.id},
        {
            id: req.body.id,
            fecha: req.body.fecha,
            id_cliente: req.body.id_cliente,
            activo: req.body.activo
        },(err) =>
        {
            if(!err)
            {
                res.send("El registro se editó exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })
});

router.delete('/borrar/:id',(req,res)=>{
    modeloEncabezadoFacturas.findOneAndDelete({id:req.params.id},
        (err) =>
        {
            if(!err)
            {
                res.send("El registro se borró exitosamente");
            }
            else
            {
                res.send(err.stack);
            }
        })
    });


module.exports = router;

