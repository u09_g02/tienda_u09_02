const mongoose = require('mongoose');
const miesquema = mongoose.Schema;
const esquemaEncabezadoFacturas = new miesquema({
        id: String,
        fecha: String,
        id_cliente: String,
        activo: Boolean
})

const modeloEncabezadoFacturas = mongoose.model('encabezado_facturas',esquemaEncabezadoFacturas);

module.exports = modeloEncabezadoFacturas;