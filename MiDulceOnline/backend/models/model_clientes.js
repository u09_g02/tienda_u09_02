const mongoose = require('mongoose');
const miesquema = mongoose.Schema;

const clienteSchema = new miesquema({
    id: {type: String, required : true, trim: true, unique: true},
    userName: String,
    contrasena: String,
    correo: String,
    nombre: String,
    telefono: String,
    fechaNacimiento: Date,
    pais: String,
    direccion: String
},
{
    timestamps: true,
    versionKey: false
});

const modeloclientes = mongoose.model('clientes', clienteSchema);

module.exports = modeloclientes;
