const express = require('express');
const app = express();
const routers = require('./routers/routers.js');

// conection con la BD
const miconexionBD = require('./conexion');

// Middleware(validar contenido) --> invocacion del bodyParser
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true })); //soluciona el problema de acentos y caracteres especiales de espanol

// rutaas
app.use('/api/v1', routers);

app.get('/', function(req,res){
    res.send('<h1>El Back-End esta funcionando</h1>')
});

// Se utilizara el puerto 5000
app.listen(5000, 
    function(){
        console.log("Servidor corriendo OK! en el puerto 5000 - http://localhost:5000");
});