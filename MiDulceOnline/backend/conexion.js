const mongoose = require('mongoose');
const url = "mongodb://localhost:27017/MiDulceOnline";  //Cambiar nombre de la BD

mongoose.connect(url);
const miconexion = mongoose.connection;

miconexion.on('connected', () => {
    console.log("La conexion con la BD fue exitosa");
});

miconexion.on('error', () => {
    console.log("Error: No se pudo establecer la conexion con la BD");
});

module.exports = mongoose;