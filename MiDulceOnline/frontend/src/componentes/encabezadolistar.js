import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import EncabezadoBorrar from './encabezadoborrar';

//Metodo que contiene las tareas para listar pedidos
function EncabezadoListar()
{

    const[dataEncabezado, setdataEncabezado] = useState([])

    useEffect(()=>{
        axios.get('/api/v1/encabezado_facturas/listar').then(res => {
        console.log(res.data)
        setdataEncabezado(res.data)
        }).catch(err=>{console.log(err.stack)})
    },[])
    
    return (
        <div className="container mt-5">
        <h4>Lista de Pedidos</h4>
        <div className="row">
            <div className="col-md-12">
                <table className="table table-bordered">
                    <thead className="thead-dark">
                        <tr key={-1}>
                            <td colSpan={6} align="right"><Link to={`/encabezadoagregar`}><li className='btn btn-success'>Agregar Pedido</li></Link></td>
                        </tr>
                        <tr key={0}>
                            <td align="center">Id</td>
                            <td>Fecha</td>
                            <td align="center">Id_cliente</td>
                            <td align="center">Estado</td>
                            <td align="center"></td>
                            <td align="center"></td>
                        </tr>
                    </thead> 
                    <tbody className='dialog'>
                    {
                        dataEncabezado.map(miencabezado => (
                        <tr key={miencabezado.id}>
                            <td align="center">{miencabezado.id}</td>
                            <td>{miencabezado.fecha}</td>
                            <td align="center">{miencabezado.id_cliente}</td>
                            <td align="center">{miencabezado.activo ? 'Activo' : 'Inactivo'}</td>
                            <td align="center"><Link to={`/encabezadoeditar/${miencabezado.id}`}><li className='btn btn-info'>Editar</li></Link></td>
                            <td align="center"><li className='btn btn-danger' onClick={()=>{EncabezadoBorrar(miencabezado.id)}}>Borrar</li></td>
                        </tr>
                    ))
                    }
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    )

}
export default EncabezadoListar;

/*
const listapedidos = dataPedidos.map(mipedido => {
return(
    <div>
        <PedidosDetalle mipedido={mipedido}/>
    </div>
)
})
return(
<div>
    <h1>Lista de Pedidos</h1>
    {listapedidos}
</div>
)
*/

/*
function PedidosDetalle({mipedido})
{
return(
<div className="container">
<table className="table">
<tbody id="tablapedidos">
<tr>
<th scope="row">{mipedido.id}</th>
<td>{mipedido.fecha}</td>
<td>{mipedido.valor}</td>
<td>{mipedido.activo}</td>
</tr>
</tbody>
</table>
</div>

)
}
export default PedidosDetalle;
*/
