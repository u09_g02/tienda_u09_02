import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useParams, useNavigate} from 'react-router';
import Swal from 'sweetalert2';

/*
import Checkbox from '@material-ui/core/Checkbox';
export default function Checkboxes()
{
    const [checked, setChecked] = React.useState(true);
    const handleChange = (event) => {
    setChecked(event.target.checked);
};
<Checkbox checked={checked} onChange={handleChange} inputProps={{ 'aria-label': 'primary checkbox' }}/>
*/
function EncabezadoEditar()
{
    const parametros = useParams('')
    const[fecha, setFecha] = useState('')
    const[id_cliente, setIdCliente] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    useEffect(()=>{
        //axios.post('api/pedidos/cargardata', {id: parametros.id}).then(res => {
        axios.get(`/api/v1/encabezado_facturas/cargar/${parametros.id}`).then(res => {
        //axios.post(`/api/pedidos/cargardata/${parametros.id}`).then(res => {
        console.log(res.data[0])
        const dataEncabezado = res.data[0]
        setFecha(dataEncabezado.fecha)
        setIdCliente(dataEncabezado.id_cliente)
        setActivo(dataEncabezado.activo)
        })
    },[])

    function encabezadoActualizar()
    {
        const encabezadoactualizar = {
            id: parametros.id,
            fecha: fecha,
            id_cliente: id_cliente,
            activo: activo
        }

        console.log(encabezadoactualizar)

        axios.post(`/api/v1/encabezado_facturas/editar/${parametros.id}`,encabezadoactualizar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/encabezadolistar')
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function encabezadoRegresar()
    {
        //window.location.href="/";
        navigate('/encabezadolistar')
    }

    return(
        <div className="container mt-5">
            <h4>Pedido</h4>
            <div className="row">
                <div className="col-md-12">                
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="id_cliente" className="form-label">Id Cliente</label>
                        <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e) => {setIdCliente(e.target.value)}}></input>
                    </div>    
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={encabezadoRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={encabezadoActualizar} className="btn btn-success">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

//<div className="mb-3 form-check form-switch">
//<input className="form-check-input" type="checkbox" role="switch" id="activo"></input>
//</div>

export default EncabezadoEditar;
