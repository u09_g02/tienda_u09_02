import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';

function ActualizarProducto(id){
    const params = useParams();
	const navigate = useNavigate();

    const [categoria, setCategoria] = useState('');
    const [nombre, setNombre] = useState('');
    const [marca, setMarca] = useState('');
    const [precio, setPrecio] = useState('');
    const [imagen, setImagen] = useState('');
    const [estado, setEstado] = useState('');

    // Obtencion de los datos del producto
    useEffect(() =>{
        axios.get(`/api/v1/productos/cargar/${params.id}`).then(res => {
			const producto = res.data[0];

            // Se guardan los datos del producto en los hooks
            setCategoria(producto.categoria);
            setNombre(producto.nombre);
            setMarca(producto.marca);
            setPrecio(producto.precio);
            setImagen(producto.imagen);
            setEstado(producto.estado);
        }).catch(err => { console.log(err.stack); });
    }, []);

    // Actualizacion de datos
    function actualizarProducto(){
        const productoActualizar = {
            categoria: categoria,
            nombre: nombre,
            marca: marca,
            precio: precio,
            imagen: imagen,
            estado: estado
        }

        axios.put(`/api/v1/productos/editar/${params.id}`, productoActualizar).then(res =>{
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue actualizado exitosamente!', showConfirmButton: false, timer: 1500 });
			navigate('/productos');
        }).catch(err => { console.log(err.stack); });
    }

	function retornar(){
		navigate('/productos');
	}

    return(
			<div className="container mt-5" align="center">

				<h2 className='my-4'>Actualizar Producto</h2>
				<div className="row">
						<div className="col-md-12">

							<form>
									<div className="mb-3">
											<label htmlFor="fecha" className="form-label">Categoria</label>
											<input type="text" className="form-control" id="categoria" value={categoria} onChange={(e) => {setCategoria(e.target.value)}} />
											</div>
									<div className="mb-3">
											<label htmlFor="valor" className="form-label">Nombre</label>
											<input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}} />
									</div>
									<div className="mb-3">
											<label htmlFor="valor" className="form-label">Marca</label>
											<input type="text" className="form-control" id="marca" value={marca} onChange={(e) => {setMarca(e.target.value)}} />
									</div>
									<div className="mb-3">
											<label htmlFor="valor" className="form-label">Precio</label>
											<input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}} />
									</div>
									<div className="mb-3">
											<label htmlFor="valor" className="form-label">Imagen</label>
											<input type="text" className="form-control" id="imagen" value={imagen} onChange={(e) => {setImagen(e.target.value)}} />
									</div>
									<div className="mb-3">
											<label className="form-label" htmlFor="estado">Estado</label>
											<input type="text" className="form-control" id="estado" value={estado} onChange={(e) => {setEstado(e.target.value)}} />
									</div>
									<button type="submit" className="btn btn-primary" onClick={ retornar }>Regresar</button>
									<button type="submit" className="btn btn-success" onClick={ actualizarProducto }>Actualizar</button>
							</form>
						</div>
				</div>

			</div>

    )
}

export default ActualizarProducto;