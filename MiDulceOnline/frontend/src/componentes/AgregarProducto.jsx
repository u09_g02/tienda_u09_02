import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import uniquid from 'uniqid';

function AgregarProducto(){

    const navigate = useNavigate();

    const [categoria, setCategoria] = useState('');
    const [nombre, setNombre] = useState('');
    const [marca, setMarca] = useState('');
    const [precio, setPrecio] = useState('');
    const [imagen, setImagen] = useState('');
    const [estado, setEstado] = useState('');

    function agregarProducto(){

        const productoNuevo = {
                id: uniquid(),
                nombre:nombre,
                marca: marca,
                precio: precio,
                imagen: imagen,
                estado: estado
            }

        axios.post(`/api/v1/productos/agregar`, productoNuevo).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/productos')
        }).catch(err => { console.log(err.stack); })
    }

    function regresar(){
        navigate('/productos');
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Producto</h4>
            <div className="row">
                <div className="col-md-12">
                <div className="mb-3">
                        <label htmlFor="categoria" className="form-label">Categoria</label>
                        <input type="text" className="form-control" id="categoria" value={categoria} onChange={(e) => {setCategoria(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="nombre" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="nombre" value={nombre} onChange={(e) => {setNombre(e.target.value)}}></input>
                    </div>                
                    <div className="mb-3">
                        <label htmlFor="marca" className="form-label">Marca</label>
                        <input type="text" className="form-control" id="marca" value={marca} onChange={(e) => {setMarca(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="precio" className="form-label">Precio</label>
                        <input type="text" className="form-control" id="precio" value={precio} onChange={(e) => {setPrecio(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="imagen" className="form-label">Imagen</label>
                        <input type="text" className="form-control" id="imagen" value={imagen} onChange={(e) => {setImagen(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="estado" className="form-label">Stock</label>
                        <input type="text" className="form-control" id="estado" value={estado} onChange={(e) => {setEstado(e.target.value)}}></input>
                    </div>              
                    <div className="mb-12">
                        <button type="button" onClick={ regresar } className="btn btn-primary">Regresar</button>
                        <button type="button" onClick={ agregarProducto } className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default AgregarProducto;