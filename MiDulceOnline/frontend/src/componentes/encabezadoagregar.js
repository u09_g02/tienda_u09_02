import axios from 'axios';
import uniquid from 'uniqid';
import React, {useState} from 'react';
import {useNavigate} from 'react-router';
import Swal from 'sweetalert2';

function EncabezadoAgregar()
{
    const[fecha, setFecha] = useState('')
    const[id_cliente, setIdCliente] = useState('')
    const[activo, setActivo] = useState('') 
    const navigate = useNavigate()
    
    function encabezadoInsertar()
    {

        const encabezadoinsertar = {
            id: uniquid(),
            fecha: fecha,
            id_cliente: id_cliente,
            activo: activo
        }

        console.log(encabezadoinsertar)

        axios.post(`/api/v1/encabezado_facturas/agregar`,encabezadoinsertar).then(res => {
            console.log(res.data)
            Swal.fire({ position: 'center', icon: 'success',  title: 'El registro fue agregado exitosamente!', showConfirmButton: false, timer: 1500 })
            navigate('/encabezadoListar');
            })
            .catch(err => {console.log(err.stack)})
    
    }

    function encabezadoRegresar()
    {
        //window.location.href="/";
        navigate('/encabezadoListar');
    }

    return(
        <div className="container mt-5">
            <h4>Nuevo Pedido</h4>
            <div className="row">
                <div className="col-md-12">
                    <div className="mb-3">
                        <label htmlFor="fecha" className="form-label">Fecha</label>
                        <input type="text" className="form-control" id="fecha" value={fecha} onChange={(e) => {setFecha(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="id_cliente" className="form-label">Id_cliente</label>
                        <input type="text" className="form-control" id="id_cliente" value={id_cliente} onChange={(e) => {setIdCliente(e.target.value)}}></input>
                    </div>
                    <div className="mb-3">
                        <label htmlFor="activo" className="form-label">Activo</label>
                        <input type="text" className="form-control" id="activo" value={activo} onChange={(e) => {setActivo(e.target.value)}}></input>
                    </div>                
                    <div className="mb-12">
                        <button type="button" onClick={encabezadoRegresar} className="btn btn-primary">Atras</button>
                        <button type="button" onClick={encabezadoInsertar} className="btn btn-success">Agregar</button>
                    </div>
                </div>
            </div>
        </div>
    )

}

export default EncabezadoAgregar;
