import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import BorrarProducto from "./BorrarProducto";

function ProductoListar(){
    const [dataProductos, setDataProductos] = useState([]);

	const direccion ='api/v1/productos/borrar';

    useEffect(() => {
			axios.get('api/v1/productos/listar').then(res => { 
				console.log(res.data);
				setDataProductos(res.data);
			}).catch((err) => { console.log(err) });
    }, []);

    return(
			<div>
				<h1 align="left">Lista de Productos</h1> 
				<table className="table table-striped table-bordered">
					<thead>
					<tr key={-1}>
						<td colSpan={7} align="right"><Link to={`/productosAgregar`}><li className='btn btn-success'>Agregar Pedido</li></Link></td>
					</tr>
						<tr key={0} className="table-primary">
							<th scope="col">Id</th>
							<th scope="col">Categoria</th>
							<th scope="col">Nombre</th>
							<th scope="col">Marca</th>
							<th scope="col">Precio</th>
							<th scope="col">Estado</th>
							<th scope="col">Acciones</th>
						</tr>
					</thead>
						<tbody>
							{dataProductos.map( producto => (
								<tr key={producto.id}>
									<th scope="row">{producto.id}</th>
									<td align="center">{producto.categoria}</td>
									<td align="center">{producto.nombre}</td>
									<td align="center">{producto.marca}</td>
									<td align="right">{`$ ${ producto.precio }`}</td>
									<td align="center">{producto.estado ? "Disponible" : "Sin Stock"}</td>
									<td><Link to={`/productosEditar/${producto.id}`} ><li className="btn btn-success btn-actualizar">Editar</li></Link>
										<button className="btn btn-danger btn-borrar" onClick={() => {BorrarProducto(producto.id, direccion)} }>Borrar</button>
									</td>           
								</tr>
								))	
							}
						</tbody>
				</table> 
				
			</div>
	)
}

export default ProductoListar;