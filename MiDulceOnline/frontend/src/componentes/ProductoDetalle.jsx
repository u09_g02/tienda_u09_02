import BorrarProducto from "./BorrarProducto";
import { Link } from "react-router-dom";
import '../styles/styleProductoDetalle.css';

function ProductoDetalle({ producto }){
    const direccion ='api/v1/productos/delete';

    return(
			<table className="table table-striped table-bordered">
				<tbody>
					<tr key={producto.id}>
						<th scope="row">{producto.id}</th>
						<td align="center">{producto.categoria}</td>
						<td align="center">{producto.nombre}</td>
						<td align="center">{producto.marca}</td>
						<td align="right">{`$ ${ producto.precio }`}</td>
						<td align="center">{producto.estado ? "Disponible" : "Sin Stock"}</td>
						<td><Link to={`/productos/update/${producto.id}`} ><li className="btn btn-success btn-actualizar">Editar</li></Link>
								<button className="btn btn-danger btn-borrar" onClick={() => {BorrarProducto(producto.id, direccion)} }>Borrar</button>
						</td>            
					</tr>
				</tbody>
			</table>
    )
}

export default ProductoDetalle;