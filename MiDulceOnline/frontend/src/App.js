import { BrowserRouter, Routes, Route } from 'react-router-dom';

import ProductoListar from './componentes/ProductoListar.jsx';
import ActualizarProducto from './componentes/ActualizarProducto.jsx';
import BorrarProducto from './componentes/BorrarProducto.js';
import AgregarProducto from './componentes/AgregarProducto.jsx';
import EncabezadoListar from './componentes/encabezadolistar';
import EncabezadoBorrar from './componentes/encabezadoborrar';
import EncabezadoEditar from './componentes/encabezadoeditar';
import EncabezadoAgregar from './componentes/encabezadoagregar';

function App() {
  return (
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <a className="navbar-brand" href="/">MiDulceOnline</a>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="/navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">Inicio</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/productosListar">Productos</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="/">Clientes</a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href='/encabezadoListar'>Pedidos</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <BrowserRouter>
        <Routes>
          <Route path='/productosListar' element={<ProductoListar/>} exact></Route>
          <Route path='/productosBorrar/:id' element={<BorrarProducto/>} exact></Route>
          <Route path='/productosEditar/:id' element={<ActualizarProducto/>} exact></Route>
          <Route path='/productosAgregar' element={<AgregarProducto/>} exact></Route>
          <Route path='/encabezadoListar' element={<EncabezadoListar/>} exact></Route>
          <Route path='/encabezadoborrar/:id' element={<EncabezadoBorrar/>} exact></Route>
          <Route path='/encabezadoeditar/:id' element={<EncabezadoEditar/>} exact></Route>
          <Route path='/encabezadoagregar' element={<EncabezadoAgregar/>} exact></Route> 
        </Routes>    
      </BrowserRouter>

      <footer className="mt-5 mb-5">
          <div align="center">
              Copyright (c) 2022 - MisionTIC - Grupo U9 - Equipo 02
          </div> 
      </footer>

    </div>
  );
}

export default App;
